/*jshint esversion: 6 */


//DEFINTIONS DE TEXTE
var texte_bouton_loginregister_login = "Aller vers se connecter";
var texte_bouton_loginregister_register = "Aller vers s'inscrire";
var texte_echec_login = "Echec de connexion. Veuillez verifier vos identifiants.";
var texte_reussite_register = "Succes. Vous pouvez maintenant vous connecter."
var texte_echec_register = "Echec de l'inscription. Veuillez verifier les informations fournies.";
var texte_erreur_mot_de_passe_differents = "Les mots de passe sont différents !";


//DEFINTIONS DE TEMPLATES
const TemplateFavorie = ({ id, nom, categorie, prix }) => `

          <div class="col-auto mb-3">
            <div class="card mb-4 text-center bg-secondary text-white" style="min-width: 18rem; max-width: 18rem">             
              <div class="card-img-top image-produit-padding"> <div class="image-produit" alt="..." id="img${id}"></div> <img class="image-favori" alt="favori" src="/client/favori.png"/> </div>
              <div class="card-body">
                <!----------prend le nom de mon produit =title-->
                <h5 class="card-title">${nom}</h5>
                <p class="card-text">${categorie}</p>
                <p class="card-text">${prix}</p>
                <button id="${id}" class="btn btn-danger" onClick="retirerFavorie(this.id)">Retirer des favoris</button>
              </div>
            </div>
          </div>
      `;

const TemplatePasFavorie = ({ id, nom, categorie, prix }) => `
          <div class="col-auto mb-3">
            <div class="card mb-4 text-center bg-light" style="min-width: 18rem; max-width: 18rem">
              <div class="card-img-top image-produit-padding"><div class="image-produit" alt="..." id="img${id}"></div></div>
              <div class="card-body">
                <!----------prend le nom de mon produit =title-->
                <h5 class="card-title">${nom}</h5>
                <p class="card-text">${categorie}</p>
                <p class="card-text">${prix}</p>
                <button id="${id}" class="btn btn-primary"  onClick="ajouterFavorie(this.id)">Ajouter aux favoris</button>
              </div>
            </div>
          </div>
      `;


//HELPER
//très moche, mais c'est juste pour montrer les animations
function sleep(milliseconds) { //merci stackoverflow
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
function attendre_une_seconde_pour_faire_joli() {
    sleep(750);
}




//GLOBAL
var JWTToken; //notre token d'acces JWT
  

//Quand la page est chargée (et toute blanche car on cache le HTML avec un gros carré pendant le chargement)
$(document).ready(() => {
  initFormattage();  //on arrange la page
  storeReady(); //on prépare le store
  loginregisterReady(); //on prépare la page de connexion/register
  echangerStoreLoginregister();//appeler une première fois pour montrer le login/register

  document.getElementById("cacher-chargement").style.display="none"; //on retire le gros carré blanc qui cachait la page
});



//initialise l'état de la page initiale en cachant tout
function initFormattage(){
  document.getElementById("store").style.display="none";
  document.getElementById("login_register").style.display="none";     
}

//Echange entre la vue du store et la page de connexion/register
var loginregister_visible = false;
function echangerStoreLoginregister(){
    if(!loginregister_visible)
    {
      //affiche l'un, cache l'autre
      document.getElementById("store").style.display="none";
      document.getElementById("login_register").style.display="block";     

      loginregisterQuandAffiche(); //on prévient loginregister qu'il est affiché

      loginregister_visible = true;
    }
    else
    {
      //affiche l'un, cache l'autre
      document.getElementById("store").style.display="block";
      document.getElementById("login_register").style.display="none";      

      storeQuandAffiche(); //on prévient store qu'il est affiché

      loginregister_visible = false;
    }
     
}




//LOGIN REGISTER


//Executé au chargement de la page
function loginregisterReady() {
    
     //on défini ce que fait le bouton "Se connecter"
     $("#loginForm").submit(function (e) {
        console.log("login call");
        //requete ajax vers l'API de connexion
        $.ajax({
            type: "POST",
            url: "/Login",//API Login
            //les données du formulaires sont mises dans la requete grace à leur ID (#machin)
            data: {mail: $("#loginMail").val(), password: $("#loginPassword").val()},
            dataType: "text",
            success: function (result) { //reussite
                JWTToken = result; //Voici notre token JWT
                console.log("Succes, token "+JWTToken);
                echangerStoreLoginregister(); //afficher le store maintenant qu'on est connecté
            },
            error: function (req, status, error) { //Erreur
                alert(texte_echec_login); //Message d'échec de connexion
                console.log(req);
                console.log(status);
                console.log(error);
            }
          });
        });
    //on défini ce que fait le bouton "S'enregistrer"
    $("#registerForm").submit(function (e) {
     console.log("register call");
     //on vérifie que les mots de passe sont les mêmes
     if(document.getElementById("registerPassword").value != document.getElementById("registerConfirmedPassword").value)
     {
        alert(texte_erreur_mot_de_passe_differents);
        return;
     }
     
     //Requetes vers l'API register
     $.ajax({
                type: "POST",
                url: "/Register", //API Register
                //les données du formulaires sont mises dans la requete grace à leur ID (#machin)
                data: {nom: $("#registerNom").val(), prenom: $("#registerPrenom").val(), mail: $("#registerMail").val(), password: $("#registerPassword").val(), confirmed_password: $("#registerConfirmedPassword").val()},
                dataType: "text",
                success: function (result) { //succès
                    alert(texte_reussite_register); //Message de succès
                    console.log("Succes");
                    echangerLoginRegister(); //maintenant il faut se connecter -> afficher la page de connexion
                },
                error: function (req, status, error) { //échec
                    alert(texte_echec_register); //on affiche un message d'erreur
                    console.log(req);
                    console.log(status);
                    console.log(error);
                }
            });
    });
        
    echangerLoginRegister();// lancer une première fois pour afficher la section de connexion/register
}
//Fonction à appeler quand connexion/register devient visible
function loginregisterQuandAffiche() {   
  //rien pour loginregister
}

//Echange entre la section connexion et la section register
var login_visible = false;
function echangerLoginRegister(){
    if(!login_visible)
    {
        //échange le texte du bouton pour passer de login à register et inversement
        document.getElementById("echangerLoginRegister").value=texte_bouton_loginregister_register;

        //cache l'un, affiche l'autre
        document.getElementById("login").style.display="block";
        document.getElementById("register").style.display="none";    

        login_visible = true;
    }
    else
    {
        //échange le texte du bouton pour passer de login à register et inversement
        document.getElementById("echangerLoginRegister").value=texte_bouton_loginregister_login;

        //cache l'un, affiche l'autre
        document.getElementById("login").style.display="none";
        document.getElementById("register").style.display="block";       

        login_visible = false;
    }
}
    
//STORE

//Fonction à appeler au chargement de la page
function storeReady() {
  document.getElementById("chargementProduits").style.display="none"; //cacher le logo de chargement
  document.getElementById("erreurProduits").style.display="none"; //cacher le logo d'erreur
}

//Fonction à appeler quand store devient visible
function storeQuandAffiche() {
  chargerProduits(); //lancer le chargement et l'affichage des produits
}

//fonction qui retourne la requete exacte que l'on veut envoyer à l'API
function ajaxProduits() {
  return $.ajax({type: "GET",
                url: "/Products",
                headers: { Authorization: 'Bearer ' + JWTToken}, //on met notre code d'accès JWT
                 });
}

//fonction qui retourne la requete exacte que l'on veut envoyer à l'API
function ajaxFavories() {
  return $.ajax({type: "GET",
                 headers: { Authorization: 'Bearer ' + JWTToken }, //on met notre code d'accès JWT
                 url: "/Favories"});
}

//fonction qui retourne la requete exacte que l'on veut envoyer à l'API
function ajaxPrix() {
  return $.ajax({type: "GET",
                 headers: { Authorization: 'Bearer ' + JWTToken }, //on met notre code d'accès JWT
                 url: "/Prix"});
}

//Charge une image de manière asynchrone à partir de son ID
function chargerImage(id){
  console.log("Chargement de l'image "+id);
  //requete à l'API
  $.ajax({
    url: `/images64/${id}.jpg`, //On récupère l'image en Base64 pour pouvoir la mettre dans la page plus facilement
    headers: { Authorization: 'Bearer ' + JWTToken }, //on met notre code d'accès JWT
    success: function(image){ //succès
        console.log("Recu image "+id);
        var resultingsrc = `url('data:image/jpg;base64,${image}')`; //on créer le code CSS pour mettre l'image
        document.getElementById("img"+id).style.backgroundImage = resultingsrc; //on met le code dans le CSS
    },
    error: function(xhr, text_status){ //échec
        console.log("Failed to load image " + text_status);
    }
  });
}

//affiche les produits une fois les données reçues de l'API (Et pas avant) 
function afficherProduits(products, favories, prix) {
  console.log(products);
  console.log(favories);
  console.log(prix);
  
  //cache le logo de chargement
  document.getElementById("chargementProduits").style.display="none";
  

  //si pas de produits dans la base de données, message qui indique pas de produits avec un produti vide
  if(products.length == 0)
  {
    products = [{id: "-1",nom: "Pas de produits",categorie: ""}];
  }

  //affiche les produits
  for (var product in products) {
    console.log(product);

    // on boucle sur les produits
    const favorie = favories.includes(products[product].id); // pour savoir si un produit est Favorite  ou non

    let lePrix = prix[products[product].id].toFixed(2); // pour savoir le prix d'un produit
    if (typeof(lePrix) == "undefined") //si on a pas le prix, on met 0 pour éviter 'undefined'
    {
      lePrix = 0;
      console.log("Prix du produit " + product + " non défini !");
    }

    // les données qu'on va faire rentrer dans le tamplate
    const donnees = [
      {
        id: products[product].id,
        nom: products[product].nom,
        categorie: products[product].categorie,
        prix: `${lePrix} Euro (environ)`,
      },
    ];

    if (favorie) { //différentes templates selon si le produit est favori ou pas
      $("#emplacementProduits").append(donnees.map(TemplateFavorie));
    } else {
      $("#emplacementProduits").append(donnees.map(TemplatePasFavorie));
    }

    //demande le chargement de l'image en donnant sont ID (la fonction remplacera l'image dans la template)
    chargerImage(products[product].id);

  }


}

//, ajaxFavories(), ajaxPrix() //
function chargerProduits() {
    
  $("#emplacementProduits").empty(); // on vide tout les produits
  document.getElementById("erreurProduits").style.display="none"; //on enlève un éventuel logo d'erreur
  document.getElementById("chargementProduits").style.display="block"; //on affiche le logo de chargement

  //Requetes ajax combinées. Le code ne sera effectué que quand toutes les requetes se seront terminées.
  $.when(ajaxProduits(), ajaxFavories(), ajaxPrix()).then(function (
    resultatProducts,
    resultatFavories,
    resultatPrix
  ) {     // Execute toutes les requetes ajax vers l'API, puis attends qu'elle soit tout finies

    attendre_une_seconde_pour_faire_joli();  //pour qu'on ait le temps de voir le chargement

    console.log("Chargement ajax terminé");

    afficherProduits(resultatProducts[0], resultatFavories[0], resultatPrix[0]); // affiche les produits en passant les informations de l'API

  },function(data, textStatus, jqXHR ) { //si echec des requetes
    
    attendre_une_seconde_pour_faire_joli();  //pour qu'on ait le temps de voir le chargement

    console.log("Echec des requetes  ajax, codes:");
    console.log(textStatus); //afficher les erreurs
    console.log(jqXHR.status); //afficher les erreurs
    console.log(data.error); //afficher les erreurs
    
    //afficher le logo erreur et retirer le logo de chargement
    document.getElementById("erreurProduits").style.display="block";
    document.getElementById("chargementProduits").style.display="none";
  });
}


//STORE FONCTIONS FAVORIES
function ajouterFavorie(id) {
  $.ajax({type: "POST",
          headers: { Authorization: 'Bearer ' + JWTToken }, //on met notre code d'accès JWT
          url: `/Favorie/${id}/add`, //API Ajouter
  }).then(() => {
    chargerProduits(); //Rafraichir les produits pour voir le changement
  });
}
function retirerFavorie(id) {
  $.ajax({type: "POST",
          headers: { Authorization: 'Bearer ' + JWTToken }, //on met notre code d'accès JWT
          url: `/Favorie/${id}/remove`, //API Retirer
  }).then(() => {
    chargerProduits(); //Rafraichir les produits pour voir le changement
  });
}

