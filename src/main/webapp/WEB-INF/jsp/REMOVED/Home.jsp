<!DOCTYPE html>
<html>
  <head>
    <title>Google News Apis</title>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="/styles.css" />
    <script src="/store.js"></script>
  </head>

  <body>
    <a href="/Logout" class="logout">Logout</a>
    <div class="container mt-3">
      <div class="row">
        <div class="card-deck" id="news">
          <div class="card mb-4" style="min-width: 18rem; max-width: 18rem">
            <img
              class="card-img-top"
              src="https://placehold.it/280x140/abc"
              alt="Card image cap"
            />
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">
                This is a longer card <br />
                with <br />
                supporting <br />
                text below <br />
                as a natural lead-in to additional content. This content is a
                little bit longer.
              </p>
              <p class="card-text">
                <small class="text-muted">Last updated 3 mins ago</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
