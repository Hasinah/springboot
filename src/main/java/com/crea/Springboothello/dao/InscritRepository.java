package com.crea.Springboothello.dao;

import org.springframework.data.repository.CrudRepository;
import com.crea.Springboothello.model.Inscrit;

// This will be AUTO IMPLEMENTED by Spring into a Bean called InscritRepository
// CRUD refers Create, Read, Update, Delete

public interface InscritRepository extends CrudRepository<Inscrit, Integer> {
    //demande à spring de créer automatiquement une fonction qui cherche un inscrit par email
    Inscrit findByMail(String mail);
    Inscrit findById(int id);
    
    
}
