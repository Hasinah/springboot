
package com.crea.Springboothello.dao;

import org.springframework.data.repository.CrudRepository;
import com.crea.Springboothello.model.Product;

// This will be AUTO IMPLEMENTED by Spring into a Bean called InscritRepository
// CRUD refers Create, Read, Update, Delete

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
