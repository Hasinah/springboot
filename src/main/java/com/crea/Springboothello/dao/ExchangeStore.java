package com.crea.Springboothello.dao;
 
import javax.annotation.PostConstruct;
import org.springframework.http.*;
import org.springframework.web.reactive.function.client.*;
import org.springframework.stereotype.Component;


import com.crea.Springboothello.model.*;


//Stock les taux d'echange 
@Component
public class ExchangeStore{

	ExchangeRates exchangeRates;

  	public double get(String country) {
    	return exchangeRates.getRates().get(country);
	}

	@PostConstruct // signale que la function soit appelé 1fois
     public void telechargerTauxEchange() {
     	WebClient webClient = WebClient.builder()
            .baseUrl("https://api.exchangeratesapi.io")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            .defaultHeader(HttpHeaders.USER_AGENT, "Spring 5 WebClient")
            .build();

    	exchangeRates = webClient.get()
                .uri("/latest?base=USD")
                .retrieve()
                .bodyToMono(ExchangeRates.class).block();
	}

}
     