//import com.javadevjournal.jpa.entities.CustomerEntity;
//import com.javadevjournal.jpa.repository.CustomerRepository;
//import USER
package com.crea.Springboothello.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.crea.Springboothello.model.*;
import com.crea.Springboothello.dao.*;


//Systeme standardisé de spring qui sert à charger les parametres et autorisation d'un uilisateur
@Service
public class InscritUserDetailsService implements UserDetailsService {

    @Autowired // On récupère nos utilisateurs
    private InscritRepository inscritRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Inscrit inscritTrouve = inscritRepository.findByMail(username);
        if (inscritTrouve == null) {
            throw new UsernameNotFoundException(username);
        }
        
        //nouvel utilisateur de droit 'USER'
        UserDetails user = User.withUsername(inscritTrouve.getMail()).password(inscritTrouve.getPassword()).authorities("USER").build();
        return user;
    }

    public void inscritUtilisateur(String nom, String prenom, String mail, String password){
        Inscrit nouvelInscrit = new Inscrit();
        nouvelInscrit.setNom(nom);
        nouvelInscrit.setPrenom(prenom);
        nouvelInscrit.setMail(mail);
        nouvelInscrit.setPassword(passwordEncoder().encode(password)); // on encode le mot de passe. Spring se charge automatiquement du reste
        inscritRepository.save(nouvelInscrit);
    }

    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

} 
