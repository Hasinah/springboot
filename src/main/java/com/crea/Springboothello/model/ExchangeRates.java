package com.crea.Springboothello.model;

import java.util.Map;


//Représente tout les taux d'échanges retournés par l'API
public class ExchangeRates{
	Map<String, Double> rates;
	String base;
	String date;
	public Map<String, Double> getRates() {
		return rates;
	}
	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}