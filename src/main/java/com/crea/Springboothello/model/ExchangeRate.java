package com.crea.Springboothello.model;
 

//Représente un taux d'échange provenant de ExchangeRates (reçus de l'API)
public class ExchangeRate{
  	String name;
	double value;

  	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
     