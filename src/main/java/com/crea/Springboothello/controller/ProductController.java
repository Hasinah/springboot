package com.crea.Springboothello.controller;

import com.crea.Springboothello.dao.*;
import com.crea.Springboothello.model.Product;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.PostConstruct;

//API REST
@RestController
public class ProductController {
    //On récupère nos produits
    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(value = "/Products", method = RequestMethod.GET)
    //retourne la liste des produits
    public Iterable<Product> listeProducts() {
        return productRepository.findAll();

    }


    //FONCTION DE DEBUG, Rajoute les produits si ils manquebt dans la BD
    @PostConstruct // signale que la function soit appelé 1fois
    public void AjouterProduitsDB() {

       if(!productRepository.findAll().iterator().hasNext()) //BD vide
       {
           Product phone1 = new Product();
           phone1.setNom("Galaxy");
           phone1.setCategorie("Android");
           phone1.setPrix(300);
           productRepository.save(phone1);

           Product phone2 = new Product();
           phone2.setNom("Iphone 10");
           phone2.setCategorie("Iphone");
           phone2.setPrix(300);
           productRepository.save(phone2);

           Product phone3 = new Product();
           phone3.setNom("Iphone 10S");
           phone3.setCategorie("Iphone");
           phone3.setPrix(300);
           productRepository.save(phone3);

           Product phone4 = new Product();
           phone4.setNom("Samsung S");
           phone4.setCategorie("Android");
           phone4.setPrix(300);
           productRepository.save(phone4);
       }

    }

}
