package com.crea.Springboothello.controller;

import com.crea.Springboothello.dao.*;
import com.crea.Springboothello.model.Inscrit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

//API REST
@RestController
public class FavorieController {

    //On récupère l'accès à nos inscrits
    @Autowired 
    private InscritRepository inscritRepository;

    @RequestMapping(value = "/Favorie/{id}/add", method = RequestMethod.POST)
    //on ajoute aux favories de l'utilisatuer
    public void addFavorie(@PathVariable("id") String id, Principal principal) {

        Inscrit inscrit = inscritRepository.findByMail(principal.getName());

        if (!inscrit.getFavorie().contains(Integer.parseInt(id))) { //si il ne l'a pas déjà
            inscrit.getFavorie().add(Integer.parseInt(id)); //rajoute
        }

        inscritRepository.save(inscrit);

    }

    @RequestMapping(value = "/Favorie/{id}/remove", method = RequestMethod.POST)
    //on ajoute des favories de l'utilisatuer
    public void removeFavorie(@PathVariable("id") String id, Principal principal) {

        Inscrit inscrit = inscritRepository.findByMail(principal.getName());

        if (inscrit.getFavorie().contains(Integer.parseInt(id))) { //si il l'a 
            inscrit.getFavorie().remove(inscrit.getFavorie().indexOf(Integer.parseInt(id))); //retire
        }

        inscritRepository.save(inscrit);

    }
     

    @RequestMapping(value = "/Favories", method = RequestMethod.GET)
    // Retourne la liste des ID des produits que l'utilisateur à en favoris
    public List<Integer> isFavorie(Principal principal) {
        Inscrit inscrit = inscritRepository.findByMail(principal.getName());
        return inscrit.getFavorie();
    }

}
