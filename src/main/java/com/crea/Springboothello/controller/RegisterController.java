package com.crea.Springboothello.controller;

import com.crea.Springboothello.dao.*;
import com.crea.Springboothello.service.InscritUserDetailsService;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


//API REST
@RestController
public class RegisterController {
    //On récupère nos inscrits
    @Autowired
    private InscritRepository inscritRepository;

    //On récupère l'accès au service car il est le seul à pouvoir créer des utilisateurs (à cause du mot de passe à encoder)
    @Autowired
    private InscritUserDetailsService userDetailsService;

    // doRegister pck on peut pas faire Register GET et POST ds meme page
    // ce que fait se Connecter à DB et enregistrer la BD
    @RequestMapping(value = "/Register", method = RequestMethod.POST) // it only supports POST method
    public ResponseEntity<HttpStatus> executeRegister(@RequestParam("nom") String nom, @RequestParam("prenom") String prenom,
            @RequestParam("mail") String mail, @RequestParam("password") String password,
            @RequestParam("confirmed_password") String confirmed_password) {
                
        if (!password.equals(confirmed_password) || inscritRepository.findByMail(mail) != null) { //erreur dans les mots de passe ou déjà inscrit
            return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST); //erreur
        }

        //Demande au service InscritUserDetailsService d'inscrire l'utilisateur 
        userDetailsService.inscritUtilisateur(nom, prenom, mail, password);
        
        return new ResponseEntity<HttpStatus>(HttpStatus.OK); //Reponse réussite
    }
}
