
package com.crea.Springboothello.controller;

import com.crea.Springboothello.dao.*;
import com.crea.Springboothello.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;//affiche l'api rest
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.Map;
import java.util.HashMap;

//API REST
@RestController
// @Controller
public class PrixController {
 
    //On récupère nos produits
    @Autowired 
    private ProductRepository productRepository;

    //On récupère notre gestionnère de taux d'échanges
    @Autowired 
    private ExchangeStore exchangeStore = new ExchangeStore();


    @RequestMapping(value = "/Prix", method = RequestMethod.GET)
    //Retourne une liste des ID des produits et de leur prix ajustés selon le taux d'échange
    public Map<Integer, Double> listeProducts() {
        double tauxEchange = exchangeStore.get("EUR"); // recupere le taux d'échange


        Map<Integer, Double> lesPrix = new HashMap<Integer, Double>(); //nouvelle liste de prix
        for(Product product : productRepository.findAll()) { //pour chaque produit :
            lesPrix.put(product.getId(),product.getPrix()*tauxEchange); //on rajoute le prix dans la liste
        }

        return lesPrix;
    }


    
     


}
