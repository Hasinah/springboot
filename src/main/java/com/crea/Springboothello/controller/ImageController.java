package com.crea.Springboothello.controller;

import org.springframework.http.MediaType;
import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import javax.servlet.http.HttpServletResponse;

//API REST
@RestController
public class ImageController {


    // Permet de récupérer une image (NET SERT PLUS POUR L'INSTANT)
    @RequestMapping(value = "/images/{id}.jpg", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    // la reponse qu'on va renvoyer au Navigateur
    public void getImage(@PathVariable("id") String id, HttpServletResponse response) throws IOException {
        // recoupère les images depuis les recource

        try {
            var imgFile = new ClassPathResource("images/" + id + ".jpg");
            // le types des données par exemeples images
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            // copie les données des images et insert dans la reponse, dans le cas ou
            // l'image sont pas là lance un Exception
            StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
        } catch (Exception ex) {
            var imgFile = new ClassPathResource("images/missing.jpg");
            // le types des données par exemeples images
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            // copie les données des images et insert dans la reponse, dans le cas ou
            // l'image sont pas là lance un Exception
            StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
        }
    }

    // Permet de récupérer une image codé au format base64
    @RequestMapping(value = "/images64/{id}.jpg", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    // la reponse qu'on va renvoyer au Navigateur
    public String getImage64(@PathVariable("id") String id) throws IOException {
        // recoupère les images depuis les recource

        try {
            // le types des données par exemeples images
            byte[] fileContent = Files.readAllBytes(
                    Paths.get(this.getClass().getClassLoader().getResource("images/" + id + ".jpg").toURI()));
            String encodedString = Base64.getEncoder().encodeToString(fileContent);
            return encodedString;
        } catch (Exception ex) {
            byte[] fileContent;
            try {
                fileContent = Files.readAllBytes(
                        Paths.get(this.getClass().getClassLoader().getResource("images/missing.jpg").toURI()));
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return "";
            }

            String encodedString = Base64.getEncoder().encodeToString(fileContent);
            return encodedString;
        }
    }

}
