package com.crea.Springboothello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.crea.Springboothello.service.*;
import com.crea.Springboothello.helper.*;

@RestController
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private InscritUserDetailsService userDetailsService;

    @RequestMapping(value = "/Login", method = RequestMethod.POST)
    //Permet de se connecter
    public String createAuthenticationToken2(@RequestParam("mail") String mail, @RequestParam("password") String password) throws Exception {
        //verifie les informations fournies
        authenticate(mail, password);

        //charger l'utilisateur au format spring
        final UserDetails userDetails = userDetailsService.loadUserByUsername(mail);
        
        //créer le token d'autorisation JWT
        final String token = jwtTokenUtil.generateToken(userDetails);

        return token;
    }

    //authentifie l'utilisateur à l'aide des fonctions de Spring
    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password)); //Fonction automatique de Spring qui cherche notre utilisateur et compare sont mot de passe en utilisant notre InscritUserDetailsService
        } catch (DisabledException e) {
            throw new ResponseStatusException( HttpStatus.FORBIDDEN, "USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException( HttpStatus.FORBIDDEN, "INVALID_CREDENTIALS", e);
        }
    }
}