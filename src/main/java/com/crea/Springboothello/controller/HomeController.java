package com.crea.Springboothello.controller;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;


@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String afficherHome() {
         return "forward:/client/store.html"; //redirige vers le STORE
    }

}
