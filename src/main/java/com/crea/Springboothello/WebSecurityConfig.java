package com.crea.Springboothello;

import com.crea.Springboothello.service.InscritUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private InscritUserDetailsService NotreUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(NotreUserDetailsService).passwordEncoder(NotreUserDetailsService.passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // Desactive la protection Cross-Site Request Forgery pour le développement
        httpSecurity.csrf().disable()
        // Pas besoin d'autorisation pour ces pages
        .authorizeRequests().antMatchers("/Login", "/Register", "/", "/client/*").permitAll() // Login -> API pour se connecter, Register -> API pour s'enregistrer, / -> Pour rediriger vers le store et /client/* car tout les fichiers du store (HTML,CSS,JS,images) sont dedans
        // Toutes les autres pages nécessitent d'être connecté
        .anyRequest().authenticated().and()
        // Rajoute le message d'erreur quand non connecté
        .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
        // Rend impossible l'utilisation de sessions (bonne pratique)
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Rajoute le filtre qui verifie que l'utilisateur est connecté
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
